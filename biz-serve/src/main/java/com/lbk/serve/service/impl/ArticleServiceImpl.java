package com.lbk.serve.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lbk.mode.domain.business.Article;
import com.lbk.dao.mapper.ArticleMapper;
import com.lbk.serve.service.IArticleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  商品服务实现类
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements IArticleService {

}
