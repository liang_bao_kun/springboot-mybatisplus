package com.lbk.serve.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lbk.mode.domain.business.Order;
import com.lbk.dao.mapper.OrderMapper;
import com.lbk.serve.service.IOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  订单服务实现类
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

}
