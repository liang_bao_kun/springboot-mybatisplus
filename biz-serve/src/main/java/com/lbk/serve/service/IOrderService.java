package com.lbk.serve.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lbk.mode.domain.business.Order;

/**
 * <p>
 *  订单服务类
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
public interface IOrderService extends IService<Order> {

}
