package com.lbk.serve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lbk.mode.domain.business.Classes;
import com.lbk.mode.domain.business.Student;
import com.lbk.dao.mapper.ClassesMapper;
import com.lbk.serve.service.IClassesService;
import com.lbk.serve.service.IStudentService;
import com.lbk.mode.vo.ClassesVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xin.altitude.cms.common.util.EntityUtils;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

/**
 * <p>
 * 班级服务实现类
 * </p>
 *
 * @author LBK
 * @since 2019-12-16
 */
@Service
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, Classes> implements IClassesService {
    @Autowired
    IStudentService iStudentService;

    @Override
    public List<Classes> getList(Integer cId) {

        //Wrappers.lambdaQuery(Classes.class) 条件构造器方式
        LambdaQueryWrapper<Classes> wrappers = Wrappers.lambdaQuery(Classes.class).eq(Classes::getCId, cId);

        //Wrappers.lambdaQuery 条件构造器方式
        LambdaQueryWrapper<Classes> queryChainWrapper = new LambdaQueryWrapper<>();
        queryChainWrapper.eq(Classes::getCId, cId);
        QueryWrapper<Classes> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cId", 1);
        return this.list(queryChainWrapper);
    }


//**********************************MybatisPlus一对多条件构造器基本用法******************************************
    /**
     * 方法一：查查询班级信息(一个班级对应多个学生)
     * @param cId 班级id
     * @return 班级和对应学生的信息
     */
    public ClassesVo getOneClasses(Long cId) {

        LambdaQueryWrapper<Classes> wrappers = Wrappers.lambdaQuery(Classes.class).eq(Classes::getCId, cId);
        LambdaQueryWrapper<Classes> wrapper = new LambdaQueryWrapper<Classes>().eq(Classes::getCId, cId);
        //toObj 将对象以一种类型转换成另一种类型
        ClassesVo classesVo = EntityUtils.toObj(this.getOne(wrapper), ClassesVo::new);
        //ifPresent 有值则进行操作，ofNullable校验是否为空值
        ofNullable(classesVo).ifPresent(this::addStudentInfoList);
        return classesVo;
    }


//*****************************************第二种方法：通过orElse转换到新建的Dept对象***************************************
    /**
     * 方法二：查询班级信息(一个班级对应多个学生)
     * @param cId  班级id
     */
    public void getClass(Integer cId) {
        //LambdaQueryWrapper构造器
        LambdaQueryWrapper<Classes> wrapper = new LambdaQueryWrapper<Classes>().eq(Classes::getCId, cId);
        //.map(DeptVo::new).orElse(null)>> 查询到一个TbDept通过orElse转换到新建的Dept对象去
        //表达式写法
        // ClassesVo deptVo = Optional.ofNullable(this.getOne(wrapper)).map(ClassesVo::new).orElse(null);
        // copyProperties写法
        ClassesVo deptVo = new ClassesVo();
        BeanUtils.copyProperties(wrapper,deptVo);
        Optional.ofNullable(deptVo).ifPresent(this::addStudentInfoList);

    }

    /**
     * 根据班级id获取所有的学生
     * @param classesVo 班级vo
     */
    private void addStudentInfoList(ClassesVo classesVo) {
        //Wrappers.lambdaQuery(Student.class).eq >>>plus 3.5版本使用
        LambdaQueryWrapper<Student> wrapper = Wrappers.lambdaQuery(Student.class).eq(Student::getClass_Id, classesVo.getCId());
        List<Student> users = iStudentService.list(wrapper);
        classesVo.setStudentList(users);
    }

    /**
     * list分组
     * @param students
     * @return
     */
    public Map<String, List<Student>> groupList(List<Student> students) {

        Map<String, List<Student>> maps = new HashMap<>();
        for (Student student : students) {
            //每次获取都是map这条list
            List<Student> tmpList = maps.get(student.getName());
            if (tmpList == null) {
                tmpList = new ArrayList<>();
                tmpList.add(student);
                maps.put(student.getName(), tmpList);
            } else {
                tmpList.add(student);
            }
        }

        /*
          数组Lambda表达式分组
         */
        Map<String, List<Student>> map = students.stream().collect(Collectors.groupingBy(Student::getName));

        /*
         * forEach高级循环
         */
        map.forEach((key,value)-> {
            List<Student> sId = map.get(key);
            List<Student> sIds = value;
            sIds.forEach((is)->{
                System.out.println("....."+is.getName());
            });

        });

        return map;
    }



}
