package com.lbk.serve.service;

import com.lbk.mode.domain.business.Student;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 学生服务类
 * </p>
 *
 * @author LBK
 * @since 2019-12-16
 */
public interface IStudentService extends IService<Student> {

    List<Student> getList();

}
