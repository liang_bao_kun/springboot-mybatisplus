package com.lbk.serve.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lbk.mode.domain.business.Article;

/**
 * <p>
 *  商品服务类
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
public interface IArticleService extends IService<Article> {

}
