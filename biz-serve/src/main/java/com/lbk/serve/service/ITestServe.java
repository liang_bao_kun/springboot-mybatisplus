package com.lbk.serve.service;

/**
 * <p>
 *  测试服务类
 * </p>
 *
 * @author LBK
 * @since 2024-07-31
 */
public interface ITestServe {
    /**
     * 测试接口test
     * @return
     */
    String test();
}
