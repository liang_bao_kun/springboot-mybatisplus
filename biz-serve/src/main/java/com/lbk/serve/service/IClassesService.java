package com.lbk.serve.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lbk.mode.domain.business.Classes;

import java.util.List;

/**
 * <p>
 *  班级服务类
 * </p>
 *
 * @author LBK
 * @since 2019-12-16
 */
public interface IClassesService extends IService<Classes> {

    List<Classes> getList(Integer cId);

}
