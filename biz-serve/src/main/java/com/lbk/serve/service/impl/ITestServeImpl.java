package com.lbk.serve.service.impl;

import com.lbk.serve.service.ITestServe;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试服务实现类
 * </p>
 *
 * @author LBK
 * @since 2024-07-31
 */

@Service
public class ITestServeImpl implements ITestServe {
    @Override
    public String test() {
        return "调用serve模块接口成功";
    }
}
