package com.lbk.common.validator;

import com.lbk.common.exception.BizException;
import org.apache.commons.lang3.StringUtils;

/**
 * @Author: lbk
 * @Date: 2022/4/4 16:59
 * @Description
 * @Version 1.0
 */
public  abstract  class Assert {
    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new BizException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new BizException(message);
        }
    }
}
