package com.lbk.common.exception;

/**
 * @Author lbk
 * @Description 自定义枚举类
 * @Date 2020/12/3 11:50
 * @Version 1.0
 */
public enum CommonEnum implements  BaseErrorInfoInterface{

    /*
    * 1xx 信息，服务器收到请求，需要请求者继续执行操作。
    * 2xx 成功，操作被成功接收并处理。
    * 3xx 重定向，需要进一步的操作以完成请求。
    * 4xx 客户端错误，请求包含语法错误或者无法完成请求。
    * 5xx 服务器错误，服务器在处理请求的过程中发生了错误。
    * */

    // 数据操作错误定义
    SUCCESS("200", "成功!"),
    BODY_NOT_MATCH("400","请求的数据格式不符!"),
    SIGNATURE_NOT_MATCH("401","请求的数字签名不匹配!"),
    NOT_FOUND("404", "未找到该资源!"),
    NULL_EXCEPTION("501", "空指针异常!"),
    INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),
    SERVER_BUSY("503","服务器正忙，请稍后再试!");

    /** 错误码 */
    private String resultCode;

    /** 错误描述 */
    private String resultMsg;

    CommonEnum(String resultCode, String resultMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
    }


    @Override
    public String getResultCode() {
        return resultCode;
    }

    @Override
    public String getResultMsg() {
        return resultMsg;
    }
}
