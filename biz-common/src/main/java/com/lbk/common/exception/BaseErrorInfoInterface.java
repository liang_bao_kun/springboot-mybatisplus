package com.lbk.common.exception;

/**
 * @Author lbk
 * @Description 自定义基础接口类
 * @Date 2020/12/3 11:47
 * @Version 1.0
 */
public interface BaseErrorInfoInterface {
    /** 错误码*/
    String getResultCode();

    /** 错误描述*/
    String getResultMsg();
}
