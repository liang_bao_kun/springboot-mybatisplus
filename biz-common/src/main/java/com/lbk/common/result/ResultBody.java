package com.lbk.common.result;


import com.lbk.common.exception.BaseErrorInfoInterface;
import com.lbk.common.exception.CommonEnum;
import lombok.Data;

/**
 * @Author lbk
 * @Description 自定义数据的传输格式
 * @Date 2020/12/3 12:12
 * @Version 1.0
 */
@Data
public class ResultBody {

    /**
     * 响应代码
      */
    private String code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应结果
     */
    private Object result;

    public ResultBody(BaseErrorInfoInterface baseErrorInfoInterface) {
        this.code=baseErrorInfoInterface.getResultCode();
        this.message=baseErrorInfoInterface.getResultMsg();
    }

    public ResultBody() {

    }

    /**
     * 成功
     */
    public static ResultBody success(){
       return success(null);

    }

    /**
     * 成功
     */
    public static ResultBody success(Object data){
        ResultBody resultBody = new ResultBody();
        resultBody.setCode(CommonEnum.SUCCESS.getResultCode());
        resultBody.setMessage(CommonEnum.SUCCESS.getResultMsg());
        resultBody.setResult(data);
        return resultBody;
    }

    /**
     * 失败
     */
    public static ResultBody error(BaseErrorInfoInterface errorInfo) {
        ResultBody resultBody = new ResultBody();
        resultBody.setCode(errorInfo.getResultCode());
        resultBody.setMessage(errorInfo.getResultMsg());
        resultBody.setResult(null);
        return resultBody;
    }

    /**
     * 失败
     */
    public static ResultBody error(String code, String message) {
        ResultBody resultBody = new ResultBody();
        resultBody.setCode(code);
        resultBody.setMessage(message);
        resultBody.setResult(null);
        return resultBody;
    }

    /**
     * 失败
     */
    public static ResultBody error( String message) {
        ResultBody resultBody = new ResultBody();
        resultBody.setCode("-1");
        resultBody.setMessage(message);
        resultBody.setResult(null);
        return resultBody;
    }







}
