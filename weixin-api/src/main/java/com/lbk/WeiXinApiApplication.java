package com.lbk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 微信服务启动类
 * @author LBK
 */
@SpringBootApplication
public class WeiXinApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(WeiXinApiApplication.class, args);
    }

}
