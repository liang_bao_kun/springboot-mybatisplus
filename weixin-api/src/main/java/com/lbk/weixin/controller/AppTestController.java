package com.lbk.weixin.controller;

import com.lbk.common.result.ResultBody;
import com.lbk.weixin.annotation.Auth;
import com.lbk.weixin.aop.MyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.lbk.serve.service.IStudentService;


/**
 * @Author: lbk
 * @Date: 2022/5/14 20:37
 * @Description 微信测试接口
 * @Version 1.0
 */
@RestController
@RequestMapping("/wx-api/test")
@Api(tags = "微信模块测试接口")
public class AppTestController {

    @Autowired
    IStudentService IStudentService;

    /*测试地址: http://localhost:8989/wx-api/test/getStudentList */
    @ApiOperation("获取学生信息列表")
    @GetMapping("/getStudentList")
    public ResultBody getStudentList(){
        return ResultBody.success(IStudentService.list());
    }

    @GetMapping("/testAuth")
    @ApiOperation("测试自定义注解Auth")
    @Auth //TODO 连接点方法
    public ResultBody testAuth(){
        System.out.println("TODO正在执行testAuth测试方法");
        return ResultBody.success("测试成功");
    }

    // 文章学习https://mp.weixin.qq.com/s/t3ykX6p4u2RMxYbfFJTahg
    @GetMapping("/custom")
    @ApiOperation("测试自定义注解")
    public ResultBody custom(){
        MyService myService = new MyService();
        myService.myMethod();
        System.out.println("TODO正在执行aop测试方法");
        return ResultBody.success("测试成功");
    }

}
