package com.lbk.weixin.aop;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;
/**
 * 自定义注解处理器
 * 实现 BeanPostProcessor 接口，拦截 Bean 的初始化过程
 * @author LBK
 */
@Component
public class MyCustomAnnotationProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        // TODO 测试返回一个指定名称的class对象
        Class clazz = null;
        try {
            clazz = Class.forName("com.lbk.weixin.aop.MyService");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // 遍历 Bean 的所有方法
        for (Method method : bean.getClass().getMethods()) {
            // 如果方法上存在自定义注解
            if (method.isAnnotationPresent(LogExecutionTime.class)) {
                // 获取注解
                LogExecutionTime annotation = method.getAnnotation(LogExecutionTime.class);
                // 打印注解信息
                System.out.println("Found method: " + method.getName() + " with annotation value: ");
            }
        }
        return bean;
    }
}
