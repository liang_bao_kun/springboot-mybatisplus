package com.lbk.weixin.aop;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: LBK
 * @Date: 2024/7/30 22:31
 * @Description 自定义注解
 * @Target(ElementType.METHOD) 适用范围为方法
 * @Retention(RetentionPolicy.RUNTIME) 运行时保留
 * @Version 1.0
 */


@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LogExecutionTime {
}
