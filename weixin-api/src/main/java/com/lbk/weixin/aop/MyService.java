package com.lbk.weixin.aop;

/**
 * @Author: LBK
 * @Date: 2024/7/30 22:35
 * @Description 使用自定义注解
 * @Version 1.0
 */
public class MyService {
    @LogExecutionTime
    public void myMethod() {
        // 业务逻辑
        System.out.println("执行业务逻辑");
    }
}