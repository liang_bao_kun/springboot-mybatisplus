package com.lbk.weixin.aop;

import org.springframework.stereotype.Component;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
/**
 * @Author: LBK
 * @Date: 2024/7/30 22:32
 * @Description 创建一个实现LogExecutionTime的切面类
 * @Version 1.0
 */

@Aspect
@Component
public class LogExecutionTimeAspect {
    // @Before("@annotation(com.lbk.weixin.aop.LogExecutionTime)")
    @Before("@annotation(LogExecutionTime)")
    public void logExecutionTime() {
        System.out.println("Method execution started...Before");
    }



}