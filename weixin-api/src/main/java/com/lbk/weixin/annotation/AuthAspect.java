package com.lbk.weixin.annotation;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @Author: LBK
 * @Date: 2024/7/30 23:01
 * @Description 创建一个切面类
 * @Version https://blog.csdn.net/qq_43331014/article/details/132867178
 */
@Aspect
@Component
public class AuthAspect {
    /**
     * 定义了一个切点
     * 这里的路径填自定义注解的全路径
     */
    @Pointcut("@annotation(com.lbk.weixin.annotation.Auth)")
    public void authCut() {

    }

    @Around("authCut()")
    public Object testCutAround(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("1、@Around 注解方式AOP拦截开始进入环绕通知.......");
        Object proceed = joinPoint.proceed();
        System.out.println("3、准备退出环绕......");
        return proceed;
    }

    @Before("authCut()")
    public void cutProcess(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        System.out.println("2、@Before 注解方式AOP开始拦截, 当前拦截的方法名: " + method.getName());
    }

    @After("authCut()")
    public void after(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        System.out.println("4、@Afte 注解方式AOP执行的方法 :" + method.getName() + " 执行完了");
    }

    /**
     * returning属性指定连接点方法返回的结果放置在result变量中
     * @param joinPoint 连接点
     * @param result    返回结果
     */
    @AfterReturning(value = "authCut()", returning = "result")
    public void afterReturn(JoinPoint joinPoint, Object result) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        System.out.println("5、@AfterReturning注解方式AOP拦截的方法执行成功, 进入返回通知拦截, 方法名为: " + method.getName() + ", 返回结果为: " + result.toString());
    }

    @AfterThrowing(value = "authCut()", throwing = "e")
    public void afterThrow(JoinPoint joinPoint, Exception e) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        System.out.println("注解方式AOP进入方法异常拦截, 方法名为: " + method.getName() + ", 异常信息为: " + e.getMessage());
    }
}
