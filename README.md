#  springboot-mybatisplus

#### 项目介绍
    本项目是springboot单体架构拆分多模块，抽取出公共模块，模块与模块之间通过继承和聚合，相互关联，实现代码的复用；
    项目有两个服务端口，biz-api模块下是web接口服务，weixin-api模块下是微信接口服务

1. springBoot整合MyBatis-Plus，自定义查询xml一对多查询，mp构造器使用；
2. springBoot工程构建多模块；
3. 整合swagger接口文档
4. 配置redis工具类封装；
5. 使用java8的Lambda表达式；
6. 整合全局Exception铺抓异常返回；

#### 软件架构

1. springBoot v2.1.5
2. MyBatis-Plus v3.5.0
3. mysql v8.0
4. maven v3.9.6

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参考文档

使用框架前请阅读相关文档
<br>
>[MyBatis-Plus官网](https://baomidou.com/)



#### 模块依赖：
1.  web应用服务
    biz-api ——> biz-serve（同时依赖biz-common） ——> biz-dao ——> biz-model

2.  WeiXin应用服务
    weixin-api ——> biz-serve（同时依赖biz-common） ——> biz-dao ——> biz-model
    
