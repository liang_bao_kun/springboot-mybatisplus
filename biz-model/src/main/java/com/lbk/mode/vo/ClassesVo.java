package com.lbk.mode.vo;

import com.lbk.mode.domain.business.Classes;
import com.lbk.mode.domain.business.Student;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <p>
 * 返回前端班级实体VO
 * </p>
 * @author LBK
 * @since 2022-5-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ToString(callSuper = true)
public class ClassesVo extends Classes {

    private List<Student> studentList;

    public ClassesVo(Classes classes) {
        super(classes);
    }
    public ClassesVo() {
    }

}
