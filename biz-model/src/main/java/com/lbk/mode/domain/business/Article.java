package com.lbk.mode.domain.business;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *  商品实体 （之前不知道为什么起这个名称，懒得改了，拉了）
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "aId", type = IdType.AUTO)
    @ApiModelProperty("商品id")
    private Integer aId;

    @ApiModelProperty("商品名称")
    private String name;

    @ApiModelProperty("商品价格")
    private BigDecimal price;

    @ApiModelProperty("商品备注")
    private String remark;

    @ApiModelProperty("订单列表")
    private List<Order> orders;

}
