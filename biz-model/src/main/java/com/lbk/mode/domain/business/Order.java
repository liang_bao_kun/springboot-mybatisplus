package com.lbk.mode.domain.business;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单实体
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("oId")
    @ApiModelProperty("订单id")
    private Integer oId;

    @ApiModelProperty("订单代码")
    private String code;

    @ApiModelProperty("商品数量")
    private String total;

    @ApiModelProperty("订单日期")
    private LocalDateTime date;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("商品列表")
    private List<Article> articles;

    @TableField(exist=false)
    @ApiModelProperty("订单商品详情")
    private List<OrderArticle> orderArticleList;

}
