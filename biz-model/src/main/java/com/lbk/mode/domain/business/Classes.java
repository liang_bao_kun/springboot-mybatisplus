package com.lbk.mode.domain.business;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 班级实体
 * </p>
 *
 * @author LBK
 * @since 2019-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("classes")
public class Classes implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "cId", type = IdType.AUTO)
    private Integer cId;

    @TableField("calssName")
    private String calssName;

    @TableField("code")
    private String code;

    @TableField(exist=false)
    private List<Student> studentList;

    public Classes(Classes classes) {
        Optional.ofNullable(classes).ifPresent(e->{
            this.cId = classes.cId;
            this.calssName =  classes.calssName;
            this.code =  classes.code;
        });

    }

}
