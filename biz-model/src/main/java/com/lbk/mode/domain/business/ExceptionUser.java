package com.lbk.mode.domain.business;

import lombok.Data;
/**
 * @Author lbk
 * @Description 学生接参类（我也不知道之前写了什么）
 * @Date 2020/12/3 12:32
 * @Version 1.0
 */
@Data
public class ExceptionUser {

    private static final long serialVersionUID = 1L;
    /** 编号 */
    private int id;
    /** 姓名 */
    private String name;
    /** 年龄 */
    private int age;

}
