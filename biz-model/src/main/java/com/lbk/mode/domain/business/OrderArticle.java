package com.lbk.mode.domain.business;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Author: LBK
 * @Date: 2024/8/1 23:16
 * @Description 订单商品详细记录表
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class OrderArticle {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty("主键id")
    private Integer id;

    @ApiModelProperty("订单id")
    private Integer oId;

    @ApiModelProperty("商品id")
    private Integer aId;

    @ApiModelProperty("购买商品数量")
    private Integer num;

    @ApiModelProperty("备注")
    private String remark;


}
