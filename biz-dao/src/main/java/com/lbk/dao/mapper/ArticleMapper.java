package com.lbk.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lbk.mode.domain.business.Article;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 商品
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 根据订单id查询购买的商品
     * @param oId 订单id
     * @return 商品列表
     */
    List<Article> findArticleByoOrderId(int oId);

    /**
     * 查询商品信息
     * @param aid 商品id
     * @return 商品信息
     */
    Article findArticleId(int aid);

}
