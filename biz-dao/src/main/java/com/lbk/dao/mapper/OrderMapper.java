package com.lbk.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lbk.mode.domain.business.Order;
import com.lbk.mode.domain.business.OrderArticle;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 订单
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    /**
     * 根据商品id查询所在的订单
     * @param aId 商品id
     * @return 订单信息
     */
    List<Order> findOrderByArticleId(int aId);


    /**
     * 查询所有的订单信息 【子集查出订单详情信息】
     * @return 订单信息
     */
    List<Order> findOrderDetailedByOrderId();

    /**
     * 根据订单id，获取订单商品出售详细表
     * @param oId 订单id
     * @return 订单商品详情信息
     */
    List<OrderArticle> findOrderArticleByOrderId(int oId);

}
