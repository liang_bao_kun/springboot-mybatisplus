package com.lbk.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lbk.mode.domain.business.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 学生
 * </p>
 *
 * @author LBK
 * @since 2019-12-16
 */
@Mapper
public interface StudentMapper extends BaseMapper<Student> {

    /**
     * 查询全部学生列表，并带班级信息
     * @return 学生信息列表
     */
    List<Student> queryStudentList();

    /**
     * 根据班级的id，获取学生列表
     * @param id 班级id
     * @return 学生信息列表
     */
    List<Student> queryStudentByClazzId(int id);

}
