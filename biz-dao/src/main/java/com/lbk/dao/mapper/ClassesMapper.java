package com.lbk.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lbk.mode.domain.business.Classes;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 班级
 * </p>
 *
 * @author LBK
 * @since 2019-12-16
 */
@Mapper
public interface ClassesMapper extends BaseMapper<Classes> {

    /**
     * 查询班级列表【附带每个班级关联的学生】
     * @return 查询每个班级里的学生，对象结构，一条班级数据中，关联到的学生列表作为一个子集，如下所示
     * Classes(cId=2, calssName=计算机4班, code=144, studentList=[Student(sId=1, name=lbk, sex=男, age=20, class_Id=null, classes=null)])
     */
    List<Classes> queryClazzList();

}
