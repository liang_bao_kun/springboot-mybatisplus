package com.lbk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * web服务启动类
 * @author LBK
 */
@SpringBootApplication
public class BizApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BizApiApplication.class, args);
    }

}
