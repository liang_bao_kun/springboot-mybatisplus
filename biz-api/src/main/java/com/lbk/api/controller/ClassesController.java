package com.lbk.api.controller;

import com.lbk.serve.service.IClassesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author LBK
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/classes")
@Api(tags = "操作班级的数据接口")
public class ClassesController {
    @Autowired
    IClassesService iClassesService;

    /**
     * PathVariable 接收方式
     * @param cId 班级id
     * @return
     */
    @ApiOperation(value = "根据班级id获取班级信息", httpMethod = "GET")
    @RequestMapping(value = "/list/{cId}", method = RequestMethod.GET)
    public String getClassesList(@PathVariable Integer cId) {
        return iClassesService.getList(cId).toString();
    }

    /**
     * RequestParam 接收方式
     * @param name
     * @return
     */
    @ApiOperation(value = "测试接参方式", httpMethod = "GET")
    @GetMapping("/demo")
    public String demo(@RequestParam(name = "name") String name) {
        System.out.println("name=" + name);
        return name;
    }

}
