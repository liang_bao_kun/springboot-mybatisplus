package com.lbk.api.controller;

import com.lbk.common.utile.RedisUtil;
import com.lbk.mode.domain.business.Article;
import com.lbk.dao.mapper.ArticleMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @Author: lbk
 * @Date: 2022/4/4 16:10
 * @Description
 * @Version 1.0
 */
@RestController
@RequestMapping("/redis")
@Api(value = "/redis",tags = "redis缓存")
public class RedisController {

    @Autowired(required = false)
    RedisUtil redisUtil;
    @Autowired(required = false)
    ArticleMapper articleMapper;


    @GetMapping("/setArticle")
    @ApiOperation(value="写入article", httpMethod = "GET")
    public String setArticle() {
        BigDecimal bigDecimal = BigDecimal.valueOf(2.50);
        Article article1 =new Article();
        article1.setAId(5);
        article1.setName("lbk");
        article1.setPrice(bigDecimal);
        article1.setRemark("测试");
//        Article article=articleMapper.findArticleId(1);
        redisUtil.set("article", article1);
        return "ok";
    }


    @GetMapping("/getArticle")
    @ApiOperation(value="读取article内容", httpMethod = "GET")
    public String getArticle() {
        Article article = (Article) redisUtil.get("article");
        return article.toString();
    }


    @GetMapping("/setLBK")
    @ApiOperation(value="写入LBK内容", httpMethod = "GET")
    public String setLBK() {
        redisUtil.set("LBK", "梁保锟");
        return "setOk";
    }


    @GetMapping("/getLBK")
    @ApiOperation(value="读取LBK内容", httpMethod = "GET")
    public String getLBK() {
        return redisUtil.get("LBK").toString();
    }

    @GetMapping("/delArticle")
    @ApiOperation(value="删除article", httpMethod = "GET")
    public String delArticle() {
        redisUtil.del("article");
        return "delArticleOK";
    }

    @GetMapping("/getKeyInvalidTime")
    @ApiOperation(value="获取key获取过期时间", httpMethod = "GET")
    public String getKeyInvalidTime() {
        long b= redisUtil.getExpire("article");
        return "过期时间：" +b;
    }

}
