package com.lbk.api.controller;

import com.lbk.serve.service.ITestServe;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lbk
 */
@RestController
@RequestMapping("/test")
@Api(tags = "测试控制器")
public class TestController {
    @Autowired
    ITestServe iTestServe;

    @ApiOperation(value = "测试调用serve模块接口", httpMethod = "GET")
    @RequestMapping(value = "/getServe", method = RequestMethod.GET)
    public String getServe() {
        return iTestServe.test();
    }
}
