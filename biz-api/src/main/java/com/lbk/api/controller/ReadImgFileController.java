package com.lbk.api.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: lbk
 * @Date: 2022/12/31 12:11
 * @Description 测试读取本地指定目录下的照片
 * @Version 1.0
 */
@RestController
@RequestMapping("/file")
@Api(tags = "读取本地文件控制器")
public class ReadImgFileController {

    @RequestMapping(value = "/getImgList", method = RequestMethod.GET)
    @ApiOperation(value = "获取图片列表", httpMethod = "GET")
    public List<String> getImgList() {
        List<String> pathImgList = new ArrayList<>();
        String basePath = "D:\\Photo\\test";
        try {
            this.getFiles(basePath, pathImgList);
            pathImgList = pathImgList.stream().map(item -> item.substring(basePath.length())).collect(Collectors.toList());
            System.out.println();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pathImgList;
    }

    public void getFiles(String path, List<String> pathImgList) throws Exception {
        File file = new File(path);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File fileIndex : files) {
                //如果这个文件是目录，则进行递归搜索
                if (fileIndex.isDirectory()) {
                    getFiles(fileIndex.getPath(), pathImgList);
                } else {
                    if (checkImgFileName(fileIndex.getName())) {
                        pathImgList.add(fileIndex.getPath());
                    }
                }
            }
        }
    }


    public boolean checkImgFile(File file) {
        try {
            Image image = ImageIO.read(file);
            return image != null;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean checkImgFileName(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }
        return ("jpg".equals(extension) || "png".equals(extension));
    }
}
