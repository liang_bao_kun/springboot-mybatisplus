package com.lbk.api.controller;
import com.lbk.common.exception.BizException;
import com.lbk.mode.domain.business.ExceptionUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author lbk
 * @Description 测试异常控制器
 * @Date 2020/12/3 12:57
 * @Version 1.0
 */
@RestController
@RequestMapping(value = "/exceptionUserRest")
@Api(value = "/exceptionUserRest",tags = "测试异常控制器")
public class ExceptionUserRestController {

    @PostMapping("/users")
    @ApiOperation(value="开始新增学生信息", httpMethod = "POST")
    public boolean insert(@RequestBody ExceptionUser user) {
        System.out.println("开始新增...");
        //如果姓名为空就手动抛出一个自定义的异常！
        if(user.getName()==""){
            throw  new BizException("-1","用户姓名不能为空！");
        }
        return true;
    }

    @PutMapping("/user")
    @ApiOperation(value = "更新学生信息",httpMethod = "PUT")
    public boolean update(@RequestBody ExceptionUser user) {
        System.out.println("开始更新...");
        //这里故意造成一个空指针的异常，并且不进行处理
        String str=null;
        str.equals("111");
        return true;
    }

    @DeleteMapping("/user")
    @ApiOperation(value="开始删除学生信息", httpMethod = "POST")
    public boolean delete(@RequestBody ExceptionUser user)  {
        System.out.println("开始删除...");
        //这里故意造成一个异常，并且不进行处理
        Integer.parseInt("abc123");
        return true;
    }

    @GetMapping("/user")
    @ApiOperation(value="开始查询学生信息", httpMethod = "GET")
    public List<ExceptionUser> findByUser(ExceptionUser user) {
        System.out.println("开始查询...");
        List<ExceptionUser> userList =new ArrayList<>();
        ExceptionUser user2=new ExceptionUser();
        user2.setId(1);
        user2.setName("LBK");
        user2.setAge(18);
        userList.add(user2);
        return userList;
    }

}
