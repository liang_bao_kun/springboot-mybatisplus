package com.lbk.api.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  订单 前端控制器
 * </p>
 *
 * @author LBK
 * @since 2019-12-18
 */
@RestController
@RequestMapping("/order")
@Api(tags="订单控制器")
public class OrderController {

}
