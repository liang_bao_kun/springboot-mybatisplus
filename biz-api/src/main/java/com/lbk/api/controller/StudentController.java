package com.lbk.api.controller;

import com.lbk.mode.domain.business.Student;
import com.lbk.serve.service.IStudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LBK
 * @since 2019-12-16
 */
@RestController
@RequestMapping("/student")
@Api(tags = "学生控制器")
public class StudentController {
    @Autowired
    IStudentService iStudentService;

    @RequestMapping(value="/list", method = RequestMethod.GET)
    @ApiOperation(value="通过这个方法可以获得全部学生信息", httpMethod = "GET")
    public List<Student> studentList() {
        List<Student> students = iStudentService.list();
        for (Student s:students
        ) {
            System.out.println("学生信息："+s.toString());
        }

        return students;
    }

}
