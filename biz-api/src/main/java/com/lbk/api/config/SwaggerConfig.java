package com.lbk.api.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author lbk
 * @author lbk
 * @Description 配置Swagger接口文档
 *
 */
@Component
@Slf4j
@Configuration //在springboot中加载配置文件
/**
    * 加载swagger
    * @author lbk
* */
@EnableSwagger2
public class SwaggerConfig implements ApplicationListener<WebServerInitializedEvent> {

    /**
     * 定义服务器端口
     */
    private int serverPort;

    public int getPort() {
        return this.serverPort;
    }

    /**
     * @author lbk
     * @Description 控制台输出 Swagger 接口文档地址
     **/

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        try {
            InetAddress inetAddress = Inet4Address.getLocalHost();
            this.serverPort = event.getWebServer().getPort();
            log.info("项目启动启动成功！接口文档地址: http://"+inetAddress.getHostAddress()+":"+serverPort+"/swagger-ui.html");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo()) // 调用apiInfo方法
                .pathMapping("/") //配置访问路径
                .select()
                .paths(PathSelectors.regex("/.*")) //匹配路径下的方法
                .build();

    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder().title("")
                .contact(new Contact("example","www.baidu.com", "2305975309@qq.com"))
                .description("springboot-mybatisPlus项目生成swagger-ui接口文档")
                .version("1.0.0.0")
                .build();
    }

}