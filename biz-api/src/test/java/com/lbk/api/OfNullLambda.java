package com.lbk.api;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: lbk
 * @Date: 2022/4/23 23:54
 * @Description
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OfNullLambda {


    /**
     * 2、orElse(T other)，orElseGet(Supplier other)和orElseThrow(Supplier exceptionSupplier)
     */
//    @Test
//    public void ELSE() {
//        Student student = null;
//        //orElse和orElseGet的用法如下所示，相当于value值为null时，给予一个默认值:
//        student = Optional.ofNullable(student).orElse(newStudent());//orElse函数依然会执行createUser()方法
//        student = Optional.ofNullable(student).orElseGet(this::newStudent);//orElseGet函数并不会执行createUser()方法
//        System.out.println();
//
//        Student student1 = null;
////        try {
////            Optional.ofNullable(student1).orElseThrow(()->new Exception("用户不存在"));
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//        Optional.ofNullable(student1).orElseThrow(()-> {
//            throw new BizException("-1","用户姓名不能为空！");}
//        );
//
//    }
//
//    @Test
//    public Student newStudent(){
//        Student student = new Student();
//        student.setName("ssss");
//        return student;
//    }


}
