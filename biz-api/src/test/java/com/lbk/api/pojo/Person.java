package com.lbk.api.pojo;

import lombok.Data;

/**
 * @Author: lbk
 * @Date: 2022/4/9 11:10
 * @Description
 * @Version 1.0
 */
@Data
public class Person {

    private String elsdon;
    private String jaycob;
    private String java_programmer;
    private String male;
    private int i;
    private int i1;

    public Person(String elsdon, String jaycob, String java_programmer, String male, int i, int i1) {
        this.elsdon = elsdon;
        this.jaycob = jaycob;
        this.java_programmer = java_programmer;
        this.male = male;
        this.i = i;
        this.i1 = i1;
    }
}
