package com.lbk.api;

import com.lbk.mode.domain.business.*;
import com.lbk.dao.mapper.ArticleMapper;
import com.lbk.dao.mapper.ClassesMapper;
import com.lbk.dao.mapper.OrderMapper;
import com.lbk.dao.mapper.StudentMapper;
import com.lbk.serve.service.impl.ClassesServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyBatisTests {
    @Autowired
    StudentMapper studentMapper;
    @Autowired
    ClassesMapper classesMapper;
    @Autowired
    ArticleMapper articleMapper;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    ClassesServiceImpl classesService;

    /*************************** 【学生与班级】 *****************************************/

    /**
     * 测试查询数据库表数据
     */
    @Test
    public void TestSelectDb() {
        System.out.println("查找班级表的所有数据：");
        classesService.list().forEach(Classes->{
            System.out.println(">>>>>>>"+Classes.toString());
        });
    }

    @Test
    public void studentList() {
        System.out.println(">>>>>>>【一对一】>>>>>>>>查找所有学生信息：");
        List<Student> students = studentMapper.queryStudentList();
        for (Student s : students
        ) {
            System.out.println("学生信息：" + s.toString());
        }
    }


    @Test
    public void getStudentByClazzId() {
        System.out.println(">>>>>>>【一对多】>>>>>>>>根据班级id查找学生信息列表：");
        int b = 1;
        List<Student> students = studentMapper.queryStudentByClazzId(b);
        for (Student s : students
        ) {
            System.out.println("c_Id为"+b + "班级对应学生的信息：" + s.toString());
        }
    }

    @Test
    public void queryClazzList() {
        //查询所有的班级信息
        List<Classes> classes = classesMapper.queryClazzList();
        System.out.println(">>>>>>>>>>>>全部班级：");
        for (Classes c : classes
        ) {
            System.out.println(c.getCalssName()+":" + c.toString());
        }
    }

    /*************************** 【商品与订单】 *****************************************/

    /*************** 【订单查询】 ******************/
    @Test
    public void getOrderByArticleId() {
        System.out.println(">>>>>>>>>>>>>>>根据商品id查询所在的订单：");
        List<Order> orderList = orderMapper.findOrderByArticleId(1);
        orderList.forEach(order->{
            System.out.println("订单信息："+order.toString());
        });
    }

    @Test
    public void findOrderDetailedByOrderId() {
        System.out.println(">>>>>>>>>>>>>>>查询所有的订单信息 【子集查出订单详情信息】：");
        List<Order> orderList = orderMapper.findOrderDetailedByOrderId();
        orderList.forEach(order->{
            System.out.println("订单信息："+order.toString());
        });
    }

    @Test
    public void findOrderArticleByOrderId() {
        System.out.println(">>>>>>>>>>>>>>>根据订单id，获取订单商品出售详细表：");
        List<OrderArticle> orderArticleList = orderMapper.findOrderArticleByOrderId(1);
        orderArticleList.forEach(orderArticle->{
            System.out.println("订单商品详情表："+orderArticle.toString());
        });
    }

    /*************** 【商品查询】 ******************/
    @Test
    public void findArticleByoOrderId() {
        System.out.println(">>>>>>>>>>>>>>>根据订单id查询购买的商品：");
        List<Article> articleList = articleMapper.findArticleByoOrderId(1);
        articleList.forEach(article->{
            System.out.println("商品信息："+article.toString());
        });
    }


    @Test
    public void getArticleById() {
        System.out.println(">>>>>>>>>>>>>>>根据商品id查询商品信息：");
        Article article = articleMapper.findArticleId(1);
        System.out.println("商品信息："+article.toString());
    }

    /*************************** 【Lambda数组表达式测试】 *****************************************/

    @Test
    public void ListGourBy() {

        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student().setName("lbk").setAge(22));
        students.add(new Student().setName("lbk").setAge(56));
        students.add(new Student().setName("lzy").setAge(24));
        students.add(new Student().setName("lll").setAge(62));

        Map<String, Object> maps = new HashMap<>();
        maps.put("2", "2");
        maps.put("3", 3);
        maps.put("4", "4");
        maps.put("5", "5");

        Map<String, Object> map = new HashMap<>();
        map.put("name", "lbk");
        map.put("age", 99);
        map.put("list", students);
        map.put("map",maps);

        ArrayList<Map> maps2 = new ArrayList<>();
        maps2.add(map);
        maps2.add(map);


        classesService.groupList(students);

        //Lambda数组表达式
        Arrays.asList(new Student().setName("lbk").setAge(56),
                new Student().setName("lbk").setAge(56)).forEach(System.out::println);


        //定义一个集合存放用户角色
        List<String> role = new ArrayList<String>();
        for (int i = 0; i < students.size(); i++) {
            role.add(students.get(i).getName());
        }
        //同等
        List<String> roles = students.stream().map(Student::getName).collect(Collectors.toList());

    }


    @Test
    public void testIndex() {
        String str3 = new String("abc");
        String str4 = new String("abc");
        System.out.println(str3 == str4);
    }

}
